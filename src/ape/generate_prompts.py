import os
import random
import sys
from datetime import datetime

import openai
from automatic_prompt_engineer import ape

from src.datasets import word_datasets as wd, letter_datasets as ld
from src.experiment_analysis import experiments_base as e

"""
This class uses APE to generate prompts. 
"""

model = 'text-davinci-003'
num_prompts = 25  # default is 50
eval_rounds = 10  # default is 20
num_per_dataset = 10
template = "Question: {} : {}\nOptions:\n"


def execute_ape():
    result, demo_fn = ape.simple_ape(
        dataset=(input, output),
        eval_model=model,
        prompt_gen_model=model,
        num_prompts=num_prompts,
        eval_rounds=eval_rounds
    )
    print(result)
    return result, demo_fn


def ape_examples_words(data):
    i = []
    o = []
    for idx, line in enumerate(data):
        prompt, answer = wd.get_multiple_choice_prompt(line, template)
        i.append(prompt)
        o.append(answer)
    return i, o


def ape_examples_letters(data, num_per_problem):
    input = []
    output = []
    for i, problems in enumerate(data['all_prob']):
        for n, p in enumerate(problems):
            if n >= num_per_problem:
                break
            input.append(p)
            completions = data['all_prob_completion'][i][n]
            output.append(completions[0])
    indexes = list(range(len(input)))
    random.shuffle(indexes)
    input = [input[i] for i in indexes]
    output = [output[i] for i in indexes]
    return input, output


def create_word_analogies_input_output():
    bats = wd.BatsDataset(num_per_dataset, '../../data/bats/bats_10.jsonl')
    scan = wd.ScanDataset(num_per_dataset, '../../data/scan/scan_10.jsonl')
    ekar = wd.EkarDataset(num_per_dataset, '../../data/ekar/ekar_10.jsonl')
    input, output = ape_examples_words(bats)
    scan_input, scan_output = ape_examples_words(scan)
    ekar_input, ekar_output = ape_examples_words(ekar)
    input.extend(scan_input)
    input.extend(ekar_input)
    output.extend(scan_output)
    output.extend(ekar_output)
    indexes = list(range(len(input)))
    random.shuffle(indexes)
    input = [input[i] for i in indexes]
    output = [output[i] for i in indexes]
    return input, output


def create_letter_analogies_input_output():
    input, output = ape_examples_letters(ld.LetterStringDataset(), 3)
    return input, output


if __name__ == '__main__':

    openai.api_key = e.config()["general"]["api_key"]
    logs_folder = '../../results/letter_strings/ape_logs/'
    if not os.path.exists(logs_folder):
        os.mkdir(logs_folder)
    sys.stdout = open(
        logs_folder + 'ape_' + str(datetime.now()).replace(' ', '_').replace(':', '-').replace('.', '-') + '.log',
        mode='w')

    print("Hyperparameters:", "\nModel:", model, "\nNumber of prompts to generate:", num_prompts,
          "\nNumber of evaluation rounds:", eval_rounds, "\nNumber of examples per dataset:", num_per_dataset)

    print("Generating input-output pairs...")
    # generate input-output pairs for word analogies or letter string analogies
    input, output = create_word_analogies_input_output()
    # input, output = create_letter_analogies_input_output()

    # print the input and output
    print("\nREQUEST:", input)
    print("\nCORRECT ANSWER:", output)

    execute_ape()

    sys.stdout.close()
