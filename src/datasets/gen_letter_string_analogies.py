import numpy as np

"""
This script generates the letter-string problems for the experiments.
It was modified but originally taken from:
Webb, T., Holyoak, K. J., & Lu, H. (2022). Emergent analogical reasoning in large language
models. https://doi.org/10.48550/arXiv.2212.09196
"""

# Alphabet
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
           'w', 'x', 'y', 'z']
N_letters = len(letters)

# Problem 1: basic problem
# [a b c] [a b d]
# [i j k] [i j l]
prob1_name = 'basic'
all_prob1 = []
all_prob1_completion = []
prob1_choices = []
src_letters = letters[:4]
for tgt in range(N_letters - 3):
    tgt_letters = letters[tgt:tgt + 4]
    # Ensure no overlap between source and target sets
    if (np.expand_dims(np.array(src_letters), 1) == np.expand_dims(np.array(tgt_letters), 0)).sum() == 0:
        # Construct problem
        prob = '[' + src_letters[0] + ' ' + src_letters[1] + ' ' + src_letters[2] + '] '
        prob += '[' + src_letters[0] + ' ' + src_letters[1] + ' ' + src_letters[3] + ']\n'
        prob += '[' + tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[2] + '] ['
        all_prob1.append(prob)
        completion = tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[3] + ']'
        all_prob1_completion.append([completion])

        choice1 = tgt_letters[3] + ' ' + tgt_letters[1] + ' ' + tgt_letters[3] + ']'
        choice2 = tgt_letters[1] + ' ' + tgt_letters[0] + ' ' + tgt_letters[3] + ']'
        choice3 = tgt_letters[0] + ' ' + tgt_letters[3] + ' ' + tgt_letters[1] + ']'
        prob1_choices.append([choice1, choice2, choice3])
        print(prob + completion + " " + choice1 + " " + choice2 + " " + choice3 + "\n")
# Shuffle
all_prob1 = np.array(all_prob1)
all_prob1_completion = np.array(all_prob1_completion)
prob1_choices = np.array(prob1_choices)
random_ind = np.arange(all_prob1.shape[0])
np.random.shuffle(random_ind)
all_prob1 = all_prob1[random_ind]
all_prob1_completion = all_prob1_completion[random_ind]
prob1_choices = prob1_choices[random_ind]
print(prob1_name, "contains", len(all_prob1), "problems.\n\n")

# Problem 2: cleaning up a string 
# [a b b c d e] [a b c d e]
# [p q r r s t] [p q r s t]
prob2_name = 'remove_redundant_chars'
all_prob2 = []
all_prob2_completion = []
prob2_choices = []
src_letters = letters[:5]
for tgt in range(N_letters - 4):
    tgt_letters = letters[tgt:tgt + 5]
    # Ensure no overlap between source and target sets
    if (np.expand_dims(np.array(src_letters), 1) == np.expand_dims(np.array(tgt_letters), 0)).sum() == 0:
        # Randomly repeat one letter in source and one letter in target (not the same letter)
        repeat_ind = np.arange(1, 4)
        np.random.shuffle(repeat_ind)
        src_repeat = repeat_ind[0]
        tgt_repeat = repeat_ind[1]
        # Construct problem
        prob = '['
        for i in range(5):
            if i == src_repeat:
                prob += src_letters[i] + ' ' + src_letters[i]
            else:
                prob += src_letters[i]
            if i < 4:
                prob += ' '
            else:
                prob += '] ['
        for i in range(5):
            prob += src_letters[i]
            if i < 4:
                prob += ' '
            else:
                prob += ']\n['
        for i in range(5):
            if i == tgt_repeat:
                prob += tgt_letters[i] + ' ' + tgt_letters[i]
            else:
                prob += tgt_letters[i]
            if i < 4:
                prob += ' '
            else:
                prob += '] ['
        all_prob2.append(prob)
        completion = ''
        for i in range(5):
            completion += tgt_letters[i]
            if i < 4:
                completion += ' '
            else:
                completion += ']'
        all_prob2_completion.append([completion])

        choice1 = tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[3] + ' ' + tgt_letters[1] + ' ' + \
                  tgt_letters[3] + ']'
        choice2 = tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[3] + ' ' + tgt_letters[4] + ' ' + \
                  tgt_letters[1] + ']'
        choice3 = tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[2] + ' ' + tgt_letters[3] + ' ' + \
                  tgt_letters[1] + ']'
        prob2_choices.append([choice1, choice2, choice3])
        print(prob + completion + " " + choice1 + " " + choice2 + " " + choice3 + "\n")
# Shuffle
all_prob2 = np.array(all_prob2)
all_prob2_completion = np.array(all_prob2_completion)
prob2_choices = np.array(prob2_choices)
random_ind = np.arange(all_prob2.shape[0])
np.random.shuffle(random_ind)
all_prob2 = all_prob2[random_ind]
all_prob2_completion = all_prob2_completion[random_ind]
prob2_choices = prob2_choices[random_ind]
print(prob2_name, "contains", len(all_prob2), "problems.\n\n")

# Problem 3: abstract succesorship, version 1
# [a b c] [a b d]
# [1 2 3] [1 2 4]
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
N_numbers = len(numbers)
prob3_name = 'letter_to_num'
all_prob3 = []
all_prob3_completion = []
prob3_choices = []
# src_letters = letters[:4]
# for tgt in range(N_numbers - 3):
#     tgt_numbers = numbers[tgt:tgt + 4]
#     # Construct problem
#     prob = '[' + src_letters[0] + ' ' + src_letters[1] + ' ' + src_letters[2] + '] '
#     prob += '[' + src_letters[0] + ' ' + src_letters[1] + ' ' + src_letters[3] + ']\n'
#     prob += '[' + tgt_numbers[0] + ' ' + tgt_numbers[1] + ' ' + tgt_numbers[2] + '] ['
#     all_prob3.append(prob)
#     completion = tgt_numbers[0] + ' ' + tgt_numbers[1] + ' ' + tgt_numbers[3] + ']'
#     all_prob3_completion.append([completion])
#
#     choice1 = tgt_numbers[0] + ' ' + tgt_numbers[1] + ' ' + tgt_numbers[2] + ']'
#     choice2 = tgt_numbers[1] + ' ' + tgt_numbers[2] + ' ' + tgt_numbers[3] + ']'
#     choice3 = tgt_numbers[3] + ' ' + tgt_numbers[2] + ' ' + tgt_numbers[1] + ']'
#     prob3_choices.append([choice1, choice2, choice3])
#     print(prob + completion + " " + choice1 + " " + choice2 + " " + choice3 + "\n")
#
for letters_slice in [letters[:4], letters[4:8], letters[8:12]]:
    for tgt in range(N_numbers - 3):
        tgt_numbers = numbers[tgt:tgt + 4]
        # Construct problem
        prob = '[' + letters_slice[0] + ' ' + letters_slice[1] + ' ' + letters_slice[2] + '] '
        prob += '[' + letters_slice[0] + ' ' + letters_slice[1] + ' ' + letters_slice[3] + ']\n'
        prob += '[' + tgt_numbers[0] + ' ' + tgt_numbers[1] + ' ' + tgt_numbers[2] + '] ['
        all_prob3.append(prob)
        completion = tgt_numbers[0] + ' ' + tgt_numbers[1] + ' ' + tgt_numbers[3] + ']'
        all_prob3_completion.append([completion])

        choice1 = tgt_numbers[0] + ' ' + tgt_numbers[1] + ' ' + tgt_numbers[2] + ']'
        choice2 = tgt_numbers[1] + ' ' + tgt_numbers[2] + ' ' + tgt_numbers[3] + ']'
        choice3 = tgt_numbers[3] + ' ' + tgt_numbers[2] + ' ' + tgt_numbers[1] + ']'
        prob3_choices.append([choice1, choice2, choice3])
        print(prob + completion + " " + choice1 + " " + choice2 + " " + choice3 + "\n")

# Shuffle
all_prob3 = np.array(all_prob3)
all_prob3_completion = np.array(all_prob3_completion)
prob3_choices = np.array(prob3_choices)
random_ind = np.arange(all_prob3.shape[0])
np.random.shuffle(random_ind)
all_prob3 = all_prob3[random_ind]
all_prob3_completion = all_prob3_completion[random_ind]
prob3_choices = prob3_choices[random_ind]
print(prob3_name, "contains", len(all_prob3), "problems.\n\n")

# Problem 4: generalizing to groupings 
# [a b c] [a b d]
# [i i j j k k] [i i j j l l]
prob4_name = 'groupings'
all_prob4 = []
all_prob4_completion = []
prob4_choices = []
src_letters = letters[:4]
for tgt in range(N_letters - 3):
    tgt_letters = letters[tgt:tgt + 4]
    # Ensure no overlap between source and target sets
    if (np.expand_dims(np.array(src_letters), 1) == np.expand_dims(np.array(tgt_letters), 0)).sum() == 0:
        # Construct problem
        prob = '[' + src_letters[0] + ' ' + src_letters[1] + ' ' + src_letters[2] + '] '
        prob += '[' + src_letters[0] + ' ' + src_letters[1] + ' ' + src_letters[3] + ']\n'
        prob += '[' + tgt_letters[0] + ' ' + tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[1] + ' ' + \
                tgt_letters[2] + ' ' + tgt_letters[2] + '] ['
        all_prob4.append(prob)
        completion = tgt_letters[0] + ' ' + tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[1] + ' ' + \
                     tgt_letters[3] + ' ' + tgt_letters[3] + ']'
        all_prob4_completion.append([completion])

        choice1 = tgt_letters[1] + ' ' + tgt_letters[1] + ' ' + tgt_letters[1] + ' ' + tgt_letters[1] + ' ' + \
                  tgt_letters[3] + ' ' + tgt_letters[3] + ']'
        choice2 = tgt_letters[3] + ' ' + tgt_letters[3] + ' ' + tgt_letters[1] + ' ' + tgt_letters[1] + ' ' + \
                  tgt_letters[3] + ' ' + tgt_letters[3] + ']'
        choice3 = tgt_letters[1] + ' ' + tgt_letters[1] + ' ' + tgt_letters[3] + ' ' + tgt_letters[3] + ' ' + \
                  tgt_letters[0] + ' ' + tgt_letters[0] + ']'
        prob4_choices.append([choice1, choice2, choice3])
        print(prob + completion + " " + choice1 + " " + choice2 + " " + choice3 + "\n")
# Shuffle
all_prob4 = np.array(all_prob4)
all_prob4_completion = np.array(all_prob4_completion)
prob4_choices = np.array(prob4_choices)
random_ind = np.arange(all_prob4.shape[0])
np.random.shuffle(random_ind)
all_prob4 = all_prob4[random_ind]
all_prob4_completion = all_prob4_completion[random_ind]
prob4_choices = prob4_choices[random_ind]
print(prob4_name, "contains", len(all_prob4), "problems.\n\n")

# Problem 5: generalizing to strings of longer length
# [a b c] [a b d]
# [i j k l m] [i j l k l n]
prob5_name = 'longer_targ'
all_prob5 = []
all_prob5_completion = []
prob5_choices = []
src_letters = letters[:4]
for tgt in range(N_letters - 5):
    tgt_letters = letters[tgt:tgt + 6]
    # Ensure no overlap between source and target sets
    if (np.expand_dims(np.array(src_letters), 1) == np.expand_dims(np.array(tgt_letters), 0)).sum() == 0:
        # Construct problem
        prob = '[' + src_letters[0] + ' ' + src_letters[1] + ' ' + src_letters[2] + '] '
        prob += '[' + src_letters[0] + ' ' + src_letters[1] + ' ' + src_letters[3] + ']\n'
        prob += '[' + tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[2] + ' ' + tgt_letters[3] + ' ' + \
                tgt_letters[4] + '] ['
        all_prob5.append(prob)
        completion = tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[2] + ' ' + tgt_letters[3] + ' ' + \
                     tgt_letters[5] + ']'
        all_prob5_completion.append([completion])

        choice1 = tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[2] + ' ' + tgt_letters[4] + ' ' + \
                  tgt_letters[5] + ']'
        choice2 = tgt_letters[0] + ' ' + tgt_letters[1] + ' ' + tgt_letters[1] + ' ' + tgt_letters[2] + ' ' + \
                  tgt_letters[2] + ']'
        choice3 = tgt_letters[1] + ' ' + tgt_letters[2] + ' ' + tgt_letters[3] + ' ' + tgt_letters[4] + ' ' + \
                  tgt_letters[5] + ']'
        prob5_choices.append([choice1, choice2, choice3])
        print(prob + completion + " " + choice1 + " " + choice2 + " " + choice3 + "\n")
# Shuffle
all_prob5 = np.array(all_prob5)
all_prob5_completion = np.array(all_prob5_completion)
prob5_choices = np.array(prob5_choices)
random_ind = np.arange(all_prob5.shape[0])
np.random.shuffle(random_ind)
all_prob5 = all_prob5[random_ind]
all_prob5_completion = all_prob5_completion[random_ind]
prob5_choices = prob5_choices[random_ind]
print(prob5_name, "contains", len(all_prob5), "problems.\n\n")

# All problems
all_prob = np.array([all_prob1, all_prob2, all_prob3, all_prob4, all_prob5], dtype=object)
all_prob_completion = np.array(
    [all_prob1_completion, all_prob2_completion, all_prob3_completion, all_prob4_completion, all_prob5_completion],
    dtype=object)
all_prob_names = np.array([prob1_name, prob2_name, prob3_name, prob4_name, prob5_name])
all_choices = np.array([prob1_choices, prob2_choices, prob3_choices, prob4_choices, prob5_choices], dtype=object)

# Save all problems
np.savez('../../data/letter_string/letter_string_analogies.npz', all_prob=all_prob,
         all_prob_completion=all_prob_completion,
         all_prob_names=all_prob_names, all_choices=all_choices)
