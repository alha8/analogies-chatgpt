import numpy as np

from src.experiment_analysis.experiments_base import print_red

"""
This script contains the letter-string dataset class. 
"""


class LetterStringDataset:
    """
    A class to represent the LetterString dataset.
    """
    target_basepath = "../../data/letter_string"
    original_file = "{}/letter_string_analogies.npz".format(target_basepath)

    def __init__(self):
        self.data = np.load(self.original_file, allow_pickle=True)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        return self.data[index]

    def __iter__(self):
        return iter(self.data)


def get_multiple_choice_prompt(prompt, answer, choices):
    prompt += "Options:\n"
    try:
        choices = np.append(choices, answer)
        np.random.shuffle(choices)
        for i, c in enumerate(choices):
            # prompt += choices[i] + "\n"
            prompt += str(i) + ". " + choices[i] + "\n"
    except IndexError:
        print_red("Index error occurred: " + prompt)
        return None
    return prompt
