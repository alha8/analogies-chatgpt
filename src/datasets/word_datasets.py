import csv
import json
import os
import random
import string

from src.experiment_analysis.experiments_base import print_red

"""
This script contains the word analogy classes for each dataset (BATS, SCAN, E-KAR)
to allow simpler usage in the experiments. 
"""


class BatsDataset:
    """
    A class to represent the BATS dataset.
    """
    target_basepath = "../../data/bats"
    original_file = "{}/bats.jsonl".format(target_basepath)

    def __init__(self, num_examples, run_number, source_file=original_file):
        self.name = "bats"
        target_file = "{}/{}_{}_run_{}.jsonl".format(self.target_basepath, self.name, str(num_examples), str(run_number))
        if not os.path.exists(target_file):
            data = read_jsonl(source_file)
            random_items = random.sample(list(data), num_examples)
            with open(target_file, "w") as f:
                f.write("\n".join([json.dumps(i) for i in random_items]))
        # always read the target file (never the original_file)
        self.data = read_jsonl(target_file)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        return self.data[index]

    def __iter__(self):
        return iter(self.data)


class ScanDataset:
    """
    A class to represent the SCAN dataset.
    """
    source_basepath = "../../data/scan/source/"
    target_basepath = "../../data/scan"
    original_file = "{}/scan.jsonl".format(target_basepath)

    def __init__(self, num_examples, run_number, source_file=original_file):
        if not os.path.exists(self.original_file):
            self.convert("scan_dataset.jsonl")

        self.name = "scan"
        target_file = "{}/{}_{}_run_{}.jsonl".format(self.target_basepath, self.name, str(num_examples), str(run_number))
        if not os.path.exists(target_file):
            data = read_jsonl(source_file)
            random_items = random.sample(list(data), num_examples)
            with open(target_file, "w") as f:
                f.write("\n".join([json.dumps(i) for i in random_items]))

        # always read the target file (never the original_file)
        self.data = read_jsonl(target_file)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        return self.data[index]

    def __iter__(self):
        return iter(self.data)

    def convert(self, file):
        print("Converting SCAN dataset for file:", file)
        original = read_jsonl(self.source_basepath + file)
        result = []
        for i in original:
            src_word = i['src_word']
            source = i['source']
            targ_word = i['targ_word']
            target = i['target']

            # build the choices by getting random words from relations that are not related to the current problem
            original_without_current = []
            for o in original:
                if target != o["target"]:
                    original_without_current.append(o)
            choices = [[targ_word, target]]
            random_items = random.sample(original_without_current, 3)
            for c in random_items:
                choices.append([c['targ_word'], c['target']])
            random.shuffle(choices)
            correct_index = choices.index([targ_word, target])

            line = {
                "stem": [src_word, source],
                "answer": correct_index,
                "choice": choices
            }
            result.append(line)

        with open(self.target_basepath + "/scan.jsonl", "a") as f:
            f.write("\n".join([json.dumps(i) for i in result]))
            f.write("\n")


class EkarDataset:
    """
    A class to represent the E-KAR dataset.
    """
    source_basepath = "../../data/ekar/source/"
    target_basepath = "../../data/ekar/"
    original_file = "{}/ekar.jsonl".format(target_basepath)

    def __init__(self, num_examples, run_number, source_file=original_file):
        if not os.path.exists(self.original_file):
            self.convert("test")
            self.convert("train")
            self.convert("validation")

        self.name = "ekar"
        target_file = "{}/{}_{}_run_{}.jsonl".format(self.target_basepath, self.name, str(num_examples), str(run_number))
        if not os.path.exists(target_file):
            data = read_jsonl(source_file)
            random_items = random.sample(list(data), num_examples)
            with open(target_file, "w") as f:
                f.write("\n".join([json.dumps(i) for i in random_items]))
        # always read the target file (never the original_file)
        self.data = read_jsonl(target_file)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        return self.data[index]

    def __iter__(self):
        return iter(self.data)

    def convert(self, file):
        print("Converting EKAR dataset for file:", file)
        count_removed = 0
        original = read_jsonl(self.source_basepath + file + ".json")
        result = []
        for i in original:
            question = i['question']
            choices = i['choices']
            if len(choices['label']) != 4:
                print_red("Not exactly 4 choices. Skipping this example!")
                count_removed += 1
            if question.count(":") == 1:
                questions = question.split(":")
                choices = [x.split(':') for x in choices['text']]
                line = {
                    "stem": [questions[0], questions[1]],
                    "answer": get_alphabet_index(i['answerKey']),
                    "choice": choices
                }
                result.append(line)
            else:
                print("Question contains a tri-word analogy:", question)
                count_removed += 1

        print("Left out", count_removed, "entries due to invalid format.")
        with open(self.target_basepath + "ekar.jsonl", "a") as f:
            f.write("\n".join([json.dumps(i) for i in result]))
            f.write("\n")


def get_alphabet_index(letter):
    # Convert the letter to lowercase
    letter = letter.lower()
    # Get the index of the letter in the lowercase alphabet
    index = string.ascii_lowercase.index(letter)
    return index


def read_jsonl(file):
    with open(file, encoding="utf8") as f:
        tmp = [json.loads(x) for x in f.read().split('\n') if len(x) > 0]
    return tmp


def get_multiple_choice_prompt(line, template, debug=False):
    original_answer = line['choice'][line['answer']]

    # Build the prompt
    choices = line['choice']
    prompt = template.format(line['stem'][0], line['stem'][1])
    try:
        answer = str(choices.index(original_answer)) + ". " + original_answer[0] + " : " + original_answer[1]
        for i, c in enumerate(choices):
            prompt += str(i) + ". " + c[0] + " : " + c[1] + "\n"
            # prompt += str(i) + ". " + c[0] + " is to " + c[1] + "\n"
        if debug:
            print("\n" + prompt)
            print("CORRECT ANSWER:", answer)
    except IndexError:
        print_red("Index error occurred: " + prompt)
        return None
    return prompt, answer


def get_generative_prompt(line, template, debug=False):
    original_answer = line['choice'][line['answer']]
    prompt = template.format(line['stem'][0], line['stem'][1])
    print("\n" + prompt)
    print("CORRECT ANSWER:", original_answer)

    # Build the prompt
    choices = line['choice']
    try:
        answer = str(choices.index(original_answer)) + ". " + original_answer[0] + " : " + original_answer[1]
        for i, c in enumerate(choices):
            prompt += str(i) + ". " + c[0] + " : " + c[1] + "\n"
        if debug:
            pass
    except IndexError:
        print_red("Index error occured: " + prompt)
        return None
    return prompt, answer


def csv_to_jsonl(csv_file_path, jsonl_file_path):
    with open(csv_file_path, 'r') as csv_file:
        csv_data = csv.DictReader(csv_file)

        with open(jsonl_file_path, 'w') as jsonl_file:
            for row in csv_data:
                json_data = json.dumps(row)
                jsonl_file.write(json_data + '\n')
