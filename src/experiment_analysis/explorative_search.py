import re

import openai

from src.datasets import word_datasets as d
from src.experiment_analysis import experiments_base as e

"""
This script is used for executing explorative search for examples where the answer is wrong but makes sense.
This makes clear that multiple-choice answers are more sensible for word analogies (see chapter 3.2). 
"""


def generate_prompt(template, row):
    request = template.format(row["stem"][0], row["stem"][1], row["choice"][row["answer"]][0])
    print("\nREQUEST: '", request, "'")
    return request


def request(prompt, debug=False):
    openai.api_key = e.config()["general"]["api_key"]
    response = call_chatgpt(prompt)
    if debug:
        print("Full RESPONSE: ", response)
    answers = []
    for c in response.choices:
        # answer = c.text
        answer = c['message']['content']
        answer = re.sub(r'[^\w\s]', '', answer)
        answer = answer.strip()
        answers.append(answer)
        print("ChatGPT RESPONSE: ", answers)
    return answers


def call_chatgpt(prompt):
    model = "gpt-3.5-turbo-0613"
    response = openai.ChatCompletion.create(
        model=model,
        # max_tokens=1,
        messages=[
            {"role": "user", "content": prompt}
        ]
    )
    return response


def eval(answers, row):
    result = False
    correct_answer = row["choice"][row["answer"]][1]
    print("CORRECT ANSWER:", correct_answer)

    # target + plural
    candidates = [correct_answer, correct_answer + "s"]
    # print("Candidates:", candidates)
    for a in answers:
        if a in candidates:
            result = True
    print("--> Answer was correct:", result)
    return result


if __name__ == '__main__':
    ds = d.BatsDataset(200, 0)
    template = "{} is to {} as {} is to"
    # template = "{} : {} :: {} : "
    i = 0
    for row in ds:
        # stop after 20 wrong results
        if i >= 20:
            break
        else:
            prompt = generate_prompt(template, row)
            answers = request(prompt)
            if not eval(answers, row):
                i += 1
