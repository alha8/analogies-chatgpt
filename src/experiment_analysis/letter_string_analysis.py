import os.path
import statistics
import textwrap

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from src.experiment_analysis import experiments_base as e
from src.experiment_analysis.word_analogies_analysis import modify_values_in_table

"""
This script contains the code to analyse the results of the experiments by creating several histograms.
"""


def get_accuracies(experiment_name):
    all_acc = []
    for i in range(experiment_repetitions):
        source_files = "{}/run_{}/{}/{}_85.npz".format(results_path, str(i), experiment_name, experiment_name)
        data = np.load(source_files, allow_pickle=True)

        accuracies_per_problem = calculate_accuracies_per_problem(data)
        all_acc.append(accuracies_per_problem)

    return all_acc


def calculate_accuracies_per_problem(data):
    accuracies_per_problem = []
    for problem_id in range(n_problems):
        correct_answers = []
        # only get the answers for that current problem
        for i in range(len(data["problem_ids"])):
            if data['problem_ids'][i] == problem_id:
                correct_answers.append(data['correct_answer'][i])

        correct_answers = np.array(correct_answers)
        accuracy = correct_answers.mean()
        accuracies_per_problem.append(accuracy)
    return accuracies_per_problem


def analyze_pre_study(experiments):
    for experiment in experiments:
        experiment_name = experiments[experiment]["name"]
        print("\nAnalyzing pre_study '", experiment_name, "'")

        source_files = "{}/pre_study/{}/{}_85.npz".format(results_path, experiment_name, experiment_name)
        data = np.load(source_files, allow_pickle=True)

        accuracies_per_problem = calculate_accuracies_per_problem(data)
        print(accuracies_per_problem)
        create_histogram(experiments[experiment], accuracies_per_problem)


def analyze_results(experiments):
    folder = '{}/diagrams_tables/'.format(results_path)
    if not os.path.exists(folder):
        os.makedirs(folder)

    table = []
    for experiment in experiments:
        experiment_name = experiments[experiment]["name"]
        print("\nAnalyzing experiment '", experiment_name, "'")

        letter_string_accuracies = get_accuracies(experiment_name)

        prompt_table = []
        mean_per_problem = []
        stdev_per_problem = []
        for prob_id in range(n_problems):
            accuracy_per_problem = []
            for run in range(experiment_repetitions):
                accuracy_per_problem.append(letter_string_accuracies[run][prob_id])
            mean_per_problem.append(statistics.mean(accuracy_per_problem))
            stdev_per_problem.append(statistics.stdev(accuracy_per_problem))

            line = accuracy_per_problem
            line.append(statistics.mean(accuracy_per_problem))
            line.append(statistics.stdev(accuracy_per_problem))
            prompt_table.append(line)

        modify_values_in_table(prompt_table)
        result_df = pd.DataFrame(
            data=prompt_table,
            columns=["Run 0", "Run 1", "Run 2", "Run 3", "Run 4", 'Mean', 'Std Dev'],
            index=['Basic successor relation', 'Removing redundant characters', 'Letter to number', 'Groupings',
                   'Longer target'],
        )
        # transpose the dataframe to look the same as the overall table
        result_df = result_df.T
        print("Resulting table:\n", result_df)
        result_df.to_latex(folder + '{}_85_table.tex'.format(experiment_name), float_format="%.2f",
                           column_format="{lC{2cm}C{2cm}C{1.5cm}C{1.5cm}C{1.5cm}C{1.5cm}C{1.5cm}}",
                           caption="Accuracy for $P_{X'}$".replace("X", experiment_name.replace("ls_prompt", "")))

        # also add the average and standard deviation of the means to the table
        avg_acc = statistics.mean(mean_per_problem)
        mean_per_problem.append(avg_acc)
        stdev = statistics.stdev(mean_per_problem)
        mean_per_problem.append(stdev)

        create_histogram(experiments[experiment], mean_per_problem, stdev_per_problem)
        table.append(mean_per_problem)

    # modify the values for the table
    modify_values_in_table(table)

    result_df = pd.DataFrame(
        data=table,
        columns=['Basic successor relation', 'Removing redundant characters', 'Letter to number', 'Groupings',
                 'Longer target', 'Mean', 'Std Dev'],
        index=["\\textcolor{red}{$P_{1'}$}", "\\textcolor{ubblue}{$P_{2'}$}", "\\textcolor{orange}{$P_{3'}$}",
               "\\textcolor{orange}{$P_{4'}$}", "\\textcolor{green}{$P_{9'}$}", "$P_{11'}$", "$P_{12'}$"
               ],
    )
    print(result_df)
    result_df.to_latex("{}/letter_string_analogies_table.tex".format(folder), float_format="%.2f",
                       column_format="{lC{1.8cm}C{1.8cm}C{1.4cm}C{1.4cm}C{1.4cm}C{1.4cm}C{1.4cm}}",
                       caption="Mean Accuracy for Letter-String Analogies",
                       label="tab:results_evaluation_ls_prompts")


def create_histogram(experiment, mean_per_problem, stdev_per_problem=None):
    """
    Creates the histogram for one experiment. 
    :param experiment:
    :param mean_per_problem:
    :param stdev_per_problem: None in case of pre_study experiments, a list otherwise
    :return:
    """
    experiment_name = experiment["name"]
    multiple_choice = experiment["use_multiple_choice"]
    template = experiment["template"]

    color = '#4682B4'
    # all_err = np.stack(all_err, 1)
    x_points = np.arange(n_problems)
    plt.xticks(x_points,
               ['Basic\nsuccessor\nrelation', 'Removing\nredundant\ncharacters', 'Letter to\nnumber', 'Groupings',
                'Longer\ntarget'], fontsize=10)

    # handle values differently for pre_study values
    yerr = None
    if stdev_per_problem is not None:
        yerr = stdev_per_problem
        avg = mean_per_problem[5]
    else:
        avg = statistics.mean(mean_per_problem[0:5])

    plt.bar(x_points, mean_per_problem[0:5], yerr=yerr, color=color, width=0.8)
    plt.ylim([0, 1])
    plt.xlabel('Problem type')
    plt.ylabel('Multiple-Choice accuracy' if multiple_choice else 'Generative accuracy')
    plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1], ['0', '20%', '40%', '60%', '80%', '100%'])
    wrapped_prompt = "\n".join(textwrap.wrap(template, width=60))
    plt.title("Letter String Analogies for $P_{" + experiment_name.replace("ls_prompt", "").replace("_", "")
              + "'}$:\n'" + wrapped_prompt + "'")
    if multiple_choice:
        plt.axhline(y=0.25, color='y', linestyle='--')
    plt.axhline(y=avg, color='r', linestyle=':')
    plt.tight_layout()
    folder = '{}/diagrams_tables/'.format(results_path)
    if not os.path.exists(folder):
        os.mkdir(folder)
    plt.savefig(folder + '{}_85.png'.format(experiment_name), dpi=300)
    plt.close()


if __name__ == '__main__':
    results_path = "../../results/letter_strings"
    n_problems = 5
    letter_string_experiments = e.config()["letter_string_analogies"]
    experiment_repetitions = e.config()["general"]["experiment_repetitions"]

    # uncomment the following to execute pre-study experiments
    analyze_pre_study(dict(list(letter_string_experiments.items())[0:2]))

    # remove pre-study experiments from the list
    analyze_results(dict(list(letter_string_experiments.items())[2:9]))
