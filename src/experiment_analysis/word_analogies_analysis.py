import os
import statistics
import textwrap

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from src.experiment_analysis import experiments_base as e

"""
This script contains the code to analyse the results of the experiments by creating several histograms.
"""


def tab_val(value_to_convert):
    return round(value_to_convert * 100, 2)


def modify_values_in_table(table):
    for i in range(len(table)):
        for j in range(len(table[i])):
            table[i][j] = tab_val(table[i][j])


def get_accuracies(experiment_name, dataset_name, num_examples):
    """
    Iter over all runs and return the accuracies accordingly.
    :param experiment_name:
    :param dataset_name:
    :param num_examples:
    :return:
    """
    accuracies = []
    for i in range(experiment_repetitions):
        source_files = '../../results/word_analogy/run_{}/{}/word_analogies_{}_{}.npz'.format(str(i), experiment_name,
                                                                                   dataset_name, num_examples)
        data = np.load(source_files, allow_pickle=True)
        correct_answers = data['correct_answer']
        accuracy = correct_answers.mean()
        accuracies.append(accuracy)
    return accuracies


def analyze_results(experiments):
    """
    This method does three things:
        1. Create the histogram for each prompt with the comparison of three datasets + standard deviation.
        2. Create the overall histogram to compare all prompts.
        3. Create the latex table.
    :param experiments:
    :return:
    """
    all_acc = []
    all_stdev = []
    table = []
    for experiment in experiments:
        experiment_name = experiments[experiment]["name"]
        num_examples = experiments[experiment]["num_examples"]
        folder = '{}/diagrams_tables/'.format(results_path)
        if not os.path.exists(folder):
            os.makedirs(folder)

        # Calculate the performance measures for each prompt
        print("\nAnalyzing experiment '", experiment_name, "'")
        bats_accuracies = get_accuracies(experiment_name, 'bats', num_examples)
        bats_mean = statistics.mean(bats_accuracies)
        bats_stddev = statistics.stdev(bats_accuracies)
        print("Accuracies for BATS:", bats_accuracies)
        scan_accuracies = get_accuracies(experiment_name, 'scan', num_examples)
        scan_mean = statistics.mean(scan_accuracies)
        scan_stddev = statistics.stdev(scan_accuracies)
        print("Accuracies for SCAN:", scan_accuracies)
        ekar_accuracies = get_accuracies(experiment_name, 'ekar', num_examples)
        ekar_mean = statistics.mean(ekar_accuracies)
        ekar_stddev = statistics.stdev(ekar_accuracies)
        print("Accuracies for E-KAR:", ekar_accuracies)

        # write a table for each prompt
        prompt_table = []
        bats_accuracies.append(bats_mean)
        bats_accuracies.append(bats_stddev)
        prompt_table.append(bats_accuracies)
        scan_accuracies.append(scan_mean)
        scan_accuracies.append(scan_stddev)
        prompt_table.append(scan_accuracies)
        ekar_accuracies.append(ekar_mean)
        ekar_accuracies.append(ekar_stddev)
        prompt_table.append(ekar_accuracies)
        modify_values_in_table(prompt_table)
        result_df = pd.DataFrame(
            data=prompt_table,
            columns=["Run 0", "Run 1", "Run 2", "Run 3", "Run 4", 'Mean', 'Std Dev'],
            index=["BATS", "SCAN", "E-KAR"],
        )
        # transpose the dataframe to look the same as the overall table
        result_df = result_df.T
        print("Resulting table:\n", result_df)
        result_df.to_latex(folder + '{}_{}_table.tex'.format(experiment_name, num_examples), float_format="%.2f",
                           caption="Accuracy for $P_{X}$".replace("X", experiment_name.replace("wa_prompt", "")),
                           column_format="lccc")

        # Plot the histograms for each prompt
        color = '#4682B4'
        means = [bats_mean, scan_mean, ekar_mean]
        yerr = [bats_stddev, scan_stddev, ekar_stddev]
        plt.bar(['BATS', 'SCAN', 'E-KAR'], means, yerr=yerr, color=color)
        plt.ylim(0, 1)
        plt.xlabel('Dataset')
        plt.ylabel('Accuracy')
        plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1], ['0', '20%', '40%', '60%', '80%', '100%'])
        wrapped_prompt = "\n".join(textwrap.wrap(experiments[experiment]["template"], width=60))
        plt.title("Word Analogies for $P_{" + experiments[experiment]["name"].replace("wa_prompt", "")
                  + "}$:\n'" + wrapped_prompt + "'")
        avg_acc = statistics.mean(means)
        print("Overall Mean for this Prompt:", avg_acc)
        plt.axhline(y=avg_acc, color='r', linestyle=':')  # draw the average line for this prompt
        plt.axhline(y=0.25, color='y', linestyle='--')  # draw the random chance probability line
        plt.tight_layout()
        plt.savefig(folder + '{}_{}.png'.format(experiment_name, num_examples), dpi=300, bbox_inches="tight")
        plt.close()

        # calculate and append the measures for using them for the overall histogram and the latex table
        all_acc.append(avg_acc)
        stdev = statistics.stdev([bats_mean, scan_mean, ekar_mean])
        print("Std-Dev of the means:", stdev)
        all_stdev.append(stdev)
        table.append([tab_val(bats_mean), tab_val(scan_mean), tab_val(ekar_mean), tab_val(avg_acc), tab_val(stdev)])

    # Plot the histogram to compare all prompts with their standard deviation
    color = '#4682B4'
    plt.bar(['$P_1$', '$P_2$', '$P_4$', '$P_5$', '$P_7$', '$P_{10}$', '$P_{11}$', '$P_{12}$'], all_acc, yerr=all_stdev, color=color)
    plt.ylim(0, 1)
    plt.ylabel('Accuracy')
    plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1], ['0', '20%', '40%', '60%', '80%', '100%'])
    plt.title("Mean Accuracy per Prompt")
    plt.tight_layout()
    f = '{}/diagrams_tables/comparison_all_prompts.png'.format(results_path)
    plt.savefig(f, dpi=300, bbox_inches="tight")
    plt.close()

    # Put all values together in a table showing all results (and output it to latex code)
    result_df = pd.DataFrame(
        data=table,
        columns=["BATS", "SCAN", "E-KAR", 'Mean', 'Std Dev'],
        index=["\\textcolor{red}{$P_1$}", "\\textcolor{ubblue}{$P_2$}", "\\textcolor{red}{$P_4$}",
         "\\textcolor{orange}{$P_5$}", "\\textcolor{red}{$P_7$}", "\\textcolor{ubblue}{$P_{10}$}", "$P_{11}$",
         "$P_{12}$"],
    )
    print("Resulting table:\n", result_df)
    result_df.to_latex("{}/diagrams_tables/word_analogies_table.tex".format(results_path), float_format="%.2f",
                       caption="Mean Accuracy for Word Analogies", column_format="lccccc",
                       label="tab:results_evaluation_wa_prompts")


if __name__ == '__main__':
    results_path = "../../results/word_analogy"
    word_analogy_experiments = e.config()["word_analogies"]
    experiment_repetitions = e.config()["general"]["experiment_repetitions"]

    analyze_results(word_analogy_experiments)
