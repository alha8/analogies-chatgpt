import json
import re
import socket

import openai
import requests.exceptions
import urllib3

"""
This is the base class for executing the experiments. 
It makes sure that for each experiment the same parameters are used and handles all possible exceptions. 
"""


def call_chatgpt(prompt, max_tokens=30, debug=False):
    openai.api_key = config()["general"]["api_key"]
    kwargs = {"model": "gpt-3.5-turbo-0613", "temperature": 0, "n": 1}
    try:
        response = openai.ChatCompletion.create(
            request_timeout=120,
            messages=[
                {"role": "user", "content": prompt}
            ],
            max_tokens=max_tokens,
            **kwargs
        )
        if len(response.choices) != 1:
            raise ValueError(f"Expected exactly one choice in the response.")
        completion = response.choices[0]['message']['content']
        completion = re.sub(r'[^\w\s]', '', completion).strip().lower()
        if debug:
            print("ChatGPT RESPONSE: ", completion)
        return completion
    except (urllib3.exceptions.ReadTimeoutError,
            requests.exceptions.Timeout,
            socket.timeout,
            requests.exceptions.ReadTimeout,
            openai.error.Timeout):
        print_red("Timeout occurred. Retrying...")
        return None
    except openai.error.APIError:
        print_red("APIError occurred. Retrying...")
        return None
    except openai.error.RateLimitError:
        print_red("RateLimitError occurred. Retrying...")
        return None
    except openai.error.ServiceUnavailableError:
        print_red("Service unavailable. Retrying...")
        return None


def print_red(message):
    print("\033[31m{}\033[0m".format(message))


def config(config_path="default.json"):
    with open("../../configs/{}".format(config_path)) as f:
        config = json.load(f)
    return config
