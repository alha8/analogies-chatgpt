import os
import re
import string
import traceback

import numpy as np

from src.datasets import word_datasets as d
from src.experiment_analysis import experiments_base as e

"""
This script executes the experiments described in the configuration (default.json) for word analogies. 
"""


def evaluate(completion, answer):
    # format of answer: ["<option_num>.", "first_word", ":", "second_word"]
    correct_words = reduce_to_words(answer)
    correct_words = [x.lower() for x in correct_words]
    completion_words = completion.split()
    completion_words = [x.lower() for x in completion_words]

    # Check if every word in the correct answer is present in the given completion
    for word in correct_words:
        if word not in completion_words:
            print("--> Answer is wrong: '", word, "' was not found")
            return False

    print("--> Answer is correct")
    return True


def reduce_to_words(lst):
    # only match words (leaving out the option number and ":")
    word_pattern = re.compile(r'\b[^\d\W]+\b')
    words_only = [word for word in lst if re.match(word_pattern, word)]

    # remove punctuation if words contains any
    # (e.g. "east, west" would otherwise be split to "east," and "west" -> this removes the comma)
    translator = str.maketrans('', '', string.punctuation)
    cleaned_list = [word.translate(translator) for word in words_only if word.translate(translator)]
    return cleaned_list


def do_experiment(data, raw_template, experiment_name, num_examples, run_number):
    try:
        template = raw_template + "Question: {} : {}\n" + "Options:\n"
        # template = raw_template + "{} is to {} as\n"
        print("Starting experiment '", experiment_name, "' with", num_examples, "examples of dataset '", data.name,
              "'.", "\nTemplate:", template)
        prompts = []
        completions = []
        correct_answer = []
        for line in data:
            prompt, answer = d.get_multiple_choice_prompt(line, template, True)
            if prompt is not None:
                completion = None
                while completion is None:
                    completion = e.call_chatgpt(prompt=prompt, debug=True)
                prompts.append(prompt)
                completions.append(completion)
                correct_answer.append(evaluate(completion, answer.split()))

        folder_name = '../../results/word_analogy/run_{}/{}/'.format(run_number, experiment_name)
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
        file_name = folder_name + 'word_analogies_{}_{}.npz'.format(data.name, num_examples)
        np.savez(file_name, prompts=prompts, completions=completions, correct_answer=correct_answer, allow_pickle=True)
        print("Saved results to file:", file_name)
    except Exception:
        e.print_red("Some fatal exception occurred.")
        e.print_red(traceback.format_exc())


def run_single_experiment(experiment, run_number):
    print("Starting run number: ", run_number)
    num_examples = experiment["num_examples"]
    do_experiment(d.BatsDataset(num_examples, run_number), experiment["template"], experiment["name"], num_examples, run_number)
    do_experiment(d.ScanDataset(num_examples, run_number), experiment["template"], experiment["name"], num_examples, run_number)
    do_experiment(d.EkarDataset(num_examples, run_number), experiment["template"], experiment["name"], num_examples, run_number)


def run_all_experiments():
    for i in range(0, experiment_repetitions):
        for experiment in experiments.values():
            run_single_experiment(experiment, i)


if __name__ == '__main__':
    experiments = e.config()["word_analogies"]
    experiment_repetitions = e.config()["general"]["experiment_repetitions"]

    # either run one experiment (again) or run all experiments as defined in default.json
    # run_single_experiment(experiments["experiment2"], 0)
    run_all_experiments()
