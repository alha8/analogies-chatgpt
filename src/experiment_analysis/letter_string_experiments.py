import os
import traceback

import numpy as np

from src.datasets import letter_datasets as d
from src.experiment_analysis import experiments_base as e

"""
This script executes the experiments described in the configuration (default.json) for letter-string analogies. 
"""


def evaluate(completion, answer):
    result = False
    # if multiple answers are provided, check them all
    for single_answer in answer:
        single_answer = single_answer.replace(']', '')

        # find exactly the answer in the completion -> if found, the index will be different from -1
        idx = completion.find(single_answer)
        if idx != -1:
            # check if the completion does not contain to many letters at the end
            if 0 <= idx < len(completion):
                result = len(completion) == idx + len(single_answer)
            else:
                result = False

    print("--> Answer is", "correct" if result else "wrong", "\n")
    return result


def do_experiment(data, raw_template, experiment_name, tries_per_example, num_per_category, multiple_choice,
                  run_number, is_pre_study=False):
    print("Starting experiment '", experiment_name, "' with", tries_per_example,
          "tries per example and dataset 'letter-string'.", "\nTemplate:", raw_template)
    try:
        prompts = []
        completions = []
        correct_answer = []
        problem_ids = []
        for problem_id, problems in enumerate(data['all_prob']):
            # if problem_id != 1:
            #    continue
            count_per_category = 0
            print("\n\n\nProblem name:", data['all_prob_names'][problem_id], "ID:", problem_id)
            for idx, problem in enumerate(problems):
                if count_per_category < num_per_category:
                    count_per_category += 1
                    answer = data['all_prob_completion'][problem_id][idx]
                    prompt = raw_template + problem + "\n"
                    if multiple_choice:
                        choices = data['all_choices'][problem_id][idx]
                        prompt = d.get_multiple_choice_prompt(prompt, answer, choices)

                    last_completion = None
                    for i in range(tries_per_example):
                        print(prompt)
                        print("CORRECT ANSWER:", answer)
                        completion = None
                        while completion is None:
                            completion = e.call_chatgpt(prompt=prompt, debug=True)
                        prompts.append(prompt)
                        completions.append(completion)
                        correct_answer.append(evaluate(completion, answer))
                        problem_ids.append(problem_id)
                        # notice if a completion is different to the previous
                        if last_completion is not None and completion != last_completion:
                            e.print_red("COMPLETION CHANGED\n")
                        last_completion = completion
                else:
                    continue
        if is_pre_study:
            folder_name = "../../results/letter_strings/pre_study/{}/{}_85.npz".format(experiment_name, experiment_name)
        else:
            folder_name = '../../results/letter_strings/run_{}/{}/'.format(run_number, experiment_name)
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
        file_name = folder_name + '{}_{}.npz'.format(experiment_name, num_per_category * tries_per_example)
        np.savez(file_name, prompts=prompts, completions=completions,
                 correct_answer=correct_answer, problem_ids=problem_ids, allow_pickle=True)
        print("Saved results to file:", file_name)
    except Exception:
        e.print_red("Some fatal exception occurred:")
        e.print_red(traceback.format_exc())


def run_single_experiment(experiment, run_number, is_pre_study):
    print("Starting run number: ", run_number)
    do_experiment(d.LetterStringDataset(), experiment["template"], experiment["name"], experiment["tries_per_example"],
                  experiment["num_per_category"], experiment["use_multiple_choice"], run_number, is_pre_study)


def run_pre_study_experiments(letter_string_experiments):
    letter_string_experiments = dict(list(letter_string_experiments.items())[0:2])
    for e in letter_string_experiments.values():
        run_single_experiment(e, 0, True)

def run_all_experiments(letter_string_experiments):
    letter_string_experiments = dict(list(letter_string_experiments.items())[2:9])
    for i in range(experiment_repetitions):
        for experiment in letter_string_experiments.values():
            run_single_experiment(experiment, i, False)


if __name__ == '__main__':
    experiments = e.config()["letter_string_analogies"]
    experiment_repetitions = e.config()["general"]["experiment_repetitions"]

    # run_pre_study_experiments(experiments)
    # run_single_experiment(experiments["experiment9"], 5, False)
    run_all_experiments(experiments)
