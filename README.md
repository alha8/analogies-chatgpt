# Semantic and Syntactic Analogies with ChatGPT
This repository contains the code for my Master's thesis. 
It is about solving semantic and syntactical analogies using ChatGPT. 

## Requirements
- Insert API Key in respective field in ``default.json`` 
- Install APE as described here: https://github.com/keirp/automatic_prompt_engineer

## Project Structure
```
- configs -> contains the configuration of the experiments
    |- default.json 
- data -> contains the datasets (source + preprocessed)
    |- bats
    |- ekar
    |- scan
    |- letter-string
- results -> contains the results + analysis of the experiments
    |- letter_strings
        |- ape_logs
        |- diagrams_tables
        |- pre_study
        |- run_*
    |- word_analogy
        |- ape_logs
        |- diagrams_tables
        |- run_*
- src -> contains the source code
    |- ape
    |- datasets
    |- experiment_analysis
```

## Datasets
- Letter-string Analogies
    - ``gen_letter_string_analogies.py`` creates the letter-string problems as `npz` file. 
    - They are already stored in `data/letter_string` folder. 
- Word Analogies 
    - Word analogy datasets are generated at experiment runtime, if not already existing in the `data` folder. 
    - The source folder contains the original data, while the `bats.jsonl`, `scan.jsonl` and `ekar.jsonl` contains the full preprocessed datasets. 
    - The dataset files `*_run_*.jsonl` are the randomly extracted analogies for each run that are automatically created if not existing and used if already existing. 

## APE
APE was used to generate prompts. The code how this is done is located in `generate_prompts.py`. The logs are stored in `results/*/ape_logs`

## Experiments 
Experiments are defined in `default.json` can be executed using the respective `*_experiments.py` and can be analysed using `*_analysis.py`. 

## Results
The results are stored in the `results` folder. The `diagrams_tables` contains the generated latex tables and histograms generated from the result data. 